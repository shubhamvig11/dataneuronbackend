const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')
const UsersDetails = require('./models/UsersDetailSchema');

require('dotenv').config();

const app = express();
const PORT = 5000;

app.use(cors({
    origin: '*'
}));


// Connect to MongoDB
mongoose
    .connect(process.env.MONGO_DB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log(`MongoDB connection established`);
    })
    .catch((error) => {
        console.error('MongoDB connection error:', error);
    });

// Middleware to parse JSON bodies
app.use(express.json());

// API endpoints

// for adding and clearing all old collection
app.post('/api/add', async (req, res) => {
    try {
        const { input1, input2, input3 } = req.body;

        const deleteResult = await UsersDetails.deleteMany({});
        if (!deleteResult) {
            console.error('Error deleting documents:', error);
        }

        const newRecord = new UsersDetails({
            input1,
            input2,
            input3
        });
        const savedRecord = await newRecord.save();

        res.status(201).json({ message: 'Record added successfully', record: savedRecord });
    } catch (error) {
        console.error('Error adding record:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});


// for updating the fields accordingly
app.put('/api/update', async (req, res) => {
    try {
        const { input1, input2, input3, collectionid } = req.body;
        const result = await UsersDetails.findByIdAndUpdate(collectionid, { input1: input1, input2: input2, input3: input3 }, { new: true });
        if (!result) {
            console.log(`Document with ID ${collectionid} not found.`);
        } else {
            res.status(201).json({ message: 'Record added successfully', record: result });
        }

    } catch (error) {
        console.error('Error in /api/update:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});


app.get('/api/get', async (req, res) => {
    try {
        // Add logic to handle GET request
        const result = await UsersDetails.findOne()
        res.status(201).json({ message: 'Record added successfully', record: result });

    } catch (error) {
        console.error('Error in /api/count:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// Start the Express server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
