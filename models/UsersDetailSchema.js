const mongoose = require('mongoose');

const usersDetailsSchema = new mongoose.Schema({
    input1: {
        type: String,
    },
    input2: {
        type: String,
    },
    input3: {
        type: String,
    },
}, {
    timestamps: true,
});

const UsersDetails = mongoose.model('UsersDetails', usersDetailsSchema);

module.exports = UsersDetails; // Correct export
